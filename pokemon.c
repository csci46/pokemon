// Keep track of Pokemon
//   Name
//   Hitpoints
//   Type (0=flying, 1=water, 2=grass, 3=ground, 4=fire)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX 10

struct pokemon
{
	char name[20];
	int hp;
	int type;
};

void printPokemon(struct pokemon p);
int byName(const void *a, const void *b);
int searchByName(const void *t, const void *elem);

int main()
{
	struct pokemon pokedex[MAX];
	
	// Get input for all the Pokemon
	int count = 0;
	for (int i = 0; i < MAX; i++)
	{
		printf("Name of Pokemon %d: ", i);
		scanf("%s", pokedex[i].name);
		if (strcmp(pokedex[i].name, "DONE") == 0) break;
		printf("Hitpoints: ");
		scanf("%d", &pokedex[i].hp);
		printf("Type: ");
		scanf("%d", &pokedex[i].type);
		count++;
	}
	
	// Sort Pokemon by name
	qsort(pokedex, count, sizeof(struct pokemon), byName);
	
	printf("----- Pokedex -----\n");
	for (int i = 0; i < count; i++)
	{
		printPokemon(pokedex[i]);
	}
	
	printf("---- Search ----\n");
	printf("Name of Pokemon to find: ");
	char find[20];
	scanf("%s", find);
	// Linear search
	/*
	for (int i = 0; i < count; i++)
	{
	    if (strcmp(find, pokedex[i].name) == 0)
	    {
	        printPokemon(pokedex[i]);
	        break;
	    }
	}
	*/
	// Binary search
	struct pokemon *found = bsearch(find, pokedex, count, sizeof(struct pokemon), searchByName);
	if (found)
	{
		printf("Found it!\n");
		printPokemon(*found);
	}
	else
	{
		printf("Not found. Awwww.\n");
	}
	
}

void printPokemon(struct pokemon p)
{
	printf("%s %d %d\n", p.name, p.hp, p.type);
}

int byName(const void *a, const void *b)
{
	struct pokemon *aa = (struct pokemon *)a;
	struct pokemon *bb = (struct pokemon *)b;
	
	return strcmp(aa->name, bb->name);
}

int searchByName(const void *t, const void *elem)
{
	char *tt = (char *)t;
	struct pokemon *eelem = (struct pokemon *)elem;
	
	return strcmp(t, eelem->name);
}