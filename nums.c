#include <stdio.h>
#include <stdlib.h>

int comp(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

int bcomp(const void *t, const void *elem)
{
    int *tt = (int *)t;
    int *eelem = (int *)elem;
    
    if (*tt == *eelem) return 0;
    else if (*tt < *eelem) return -1;
    else return 1;
}

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("Must supply a filename to read from\n");
        exit(1);
    }
    FILE *in = fopen(argv[1], "r");
    if (!in)
    {
        perror("Can't open file");
        exit(1);
    }
    
    int arrayLength = 1000;
    printf("Allocating %d entries\n", arrayLength);
    int *arr = malloc(arrayLength * sizeof(int));
    int entries = 0;
    char line[20];
    while(fgets(line, 20, in) != NULL)
    {
        int num;
        sscanf(line, "%d", &num);
        
        if (entries == arrayLength)
        {
            // Array is full. Make it bigger by 25%.
            arrayLength += (arrayLength * .25);
            printf("Reallocating space for %d entries\n", arrayLength);
            arr = realloc(arr, arrayLength * sizeof(int));
        }
        arr[entries] = num;
        entries++;
    }
    
    
    
    // Bubble sort
    /*
    for (int j = 0; j < entries; j++)
    {
        for (int i = 0; i < entries-1; i++)
        {
            if (arr[i] > arr[i+1])
            {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
            }
        }
    }
    */
    
    printf("Sorting\n");
    qsort(arr, entries, sizeof(int), comp);
    printf("Done\n");
    
    int target;
    printf("Search for: ");
    scanf("%d", &target);
    
    // Linear search
    /*
    int found = 0;
    for (int i = 0; i < entries; i++)
    {
        if (arr[i] == target)
        {
            printf("Found %d!\n", arr[i]);
            found = 1;
            break;
        }
    }
    if (found == 0)
    {
        printf("Did not find %d\n", target);
    }
    */
    
    // Binary search
    int *found = bsearch(&target, arr, entries, sizeof(int), bcomp);
    if (found != NULL)
    {
        printf("Found %d!\n", *found);
    }
    else
    {
        printf("Not found\n");
    }
    
    /*
    for (int i = 0; i < entries; i++)
    {
        printf("%d %d\n", i, arr[i]);
    }
    */
    
    free(arr);
}